#!/usr/bin/env  bash

wg_bin="/usr/local/bin/wg"
path_conf="/etc/wireguard"
srv_conf="${path_conf}/wg0.conf"
cl_conf_path="${path_conf}/client"
#cl_conf="${cl_conf_path}/wg0-client.conf"


path_keys="${path_conf}/keys"


gen_srv_key(){

	srv_priv_key=`$wg_bin genkey`
	srv_publ_key=`echo ${srv_priv_key} | $wg_bin pubkey `

}

gen_cl_key(){

#	echo
#	read -p "Please enter name client from generate keys: " cl_name
#	echo

	cl_priv_key=`$wg_bin genkey`
	cl_publ_key=`echo ${cl_priv_key} | $wg_bin pubkey`
	
}

net_srv(){

	echo
        read -p "Please enter WAN ip from work server: " srv_ip
        printf "\n WAN server ip = $srv_ip \n\n"

	echo
	read -p "Please enter port from work server: " srv_port
	printf "\n Server port = $srv_port \n\n"

	echo
	read -p "Please enter ip address from vpn net (example 10.10.10.1)" srv_net
	printf "\n VPN server ip and mask = $srv_net \n \n "

	echo
        read -p "Please enter bit mask network (example 24 from 255.255.255.0)" srv_net_mask
        printf "\n VPN server net mask = $srv_net_mask \n \n "

}

gen_srv_conf(){

	gen_srv_key

	if [[ ! -f $srv_conf ]] ; then

		printf "\nServer config not found\n"
                    
		if [[ ! -d $path_conf ]] ; then
		
			printf "\nDirectory from server config not found\n\nCreate new directory from server config\n"
			
#			printf "\nCreate new directory from server config\n"	
			mkdir -p $path_conf
											
		fi
			
		printf "\nCreate new server config\n"
                        
	fi

cat << EOF > $srv_conf
		
#WAN_IP = $srv_ip

[Interface]
Address = ${srv_net}/${srv_net_mask}
PrivateKey = $srv_priv_key
ListenPort = $srv_port

EOF
	
}

net_cl(){

	gen_cl_key

	if [[ ! -f $srv_conf ]] ; then

                printf "\nServer config not found\n"

	    	gen_srv_conf
		net_srv
	
	fi
	
	echo
        read -p "Please enter id (or login) from user: " user_id
	
	find_user_id=`grep User_id $srv_conf | awk '{print $3}' | grep $user_id`

	if [[ "$find_user_id" == "$user_id" ]] ; then

		printf "\n User if = $user_id  exits. Enter another user id \n \n "
		
		echo
	        read -p "Please enter id (or login) from user: " user_id

	fi

        printf "\n User id = $user_id \n\n"

        echo
        read -p "Please enter ip address from ${srv_net}/${srv_net_mask} net:  " user_ip
        printf "\n VPN user id $user_id ip  = $user_ip \n \n "

	find_user_ip=`grep $user_ip $srv_conf | awk '{print $3}' | grep $user_ip`

        if [[ "$find_user_ip" == "${user_ip}/${srv_net_mask}" ]] ; then

                printf "\n User id $user_id ip  exits. Enter another user ip \n \n "
		printf "\n List used ip from ${srv_net}/${srv_net_mask} \n \n "
		
		list_user_ip=`grep AllowedIPs $srv_conf | awk '{print $3}'`
		printf "\n  $list_user_ip  \n \n "

                echo
                read -p "Please enter ip address from ${srv_net}/${srv_net_mask} net:  " user_ip

        fi
	
	printf "\n VPN user id $user_id ip  = $user_ip \n \n "
	
cat << EOF >> $srv_conf


#User_id = $user_id
[Peer]
PublicKey = $cl_publ_key
AllowedIPs = ${user_ip}/32

EOF

}

gen_cl_conf(){

	net_cl

	if [[ -f $srv_conf ]] ; then
		
		read_srv_wan_ip=`grep "WAN_IP" $srv_conf | awk '{print $3}' `
		
		read_srv_port=`grep "$ListenPort" $srv_conf | awk '{print $3}' `

	    else

                printf "\nServer config not found\n"
		
	fi


	if [[ ! -d $cl_conf_path ]] ; then

                        printf "\nDirectory from client config not found\n\nCreate new directory from client config\n"

                        mkdir -p $cl_conf_path

                fi

                printf "\nCreate new client config\n"


cat << EOF > ${cl_conf_path}/${$user_id}.conf


#User_id = $user_id

[Interface]
Address = ${user_ip}/${srv_net_mask}
PrivateKey = $cl_priv_key
#DNS = 1.1.1.1

[Peer]
PublicKey = $srv_publ_key 
AllowedIPs = 0.0.0.0/0
Endpoint = ${read_srv_wan_ip}:${read_srv_port}
PersistentKeepalive = 20

EOF

}

cl_conf_pack(){

tar -cvjpf ${cl_conf_path}/${$user_id}.conf.tar.bz2  ${cl_conf_path}/${$user_id}.conf

}

pf_rules_gen(){

#  Zapros na dobavlenie pravila
#  Dopisovat' prafilo v pf.conf 
#  wg_port=13462
#  pass inet ...  $wg_port
#  
#  Zapros na generaciu pf.conf
#  ssh_port=`grep -v "^#" /etc/ssh/sshd.conf | grep "Port" | awk '{print $2 }'   [if -z $ssh_port] ; then
#                                                                                       ssh_port=22
#  esli est to peredaet znachenit dalshe config kak na vpn
#  esli net to 22 port pass inet

echo

}

#gen_srv_key
#gen_cl_key
#gen_srv_conf

#echo
#echo "Server private key = $srv_priv_key"
#echo "Server public key  = $srv_publ_key"
#echo

#echo
#echo "Name client          = $cl_name"
#echo "$cl_name private key = $cl_priv_key"
#echo "$cl_name public key  = $cl_publ_key"
#echo



#for cl in {0..4} ; do  wg genkey | tee ${cl}_pr | wg pubkey | tee ${cl}_pub ; done



